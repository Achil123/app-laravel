FROM php:7.4-apache
MAINTAINER faaiqamaruadb
RUN apt update && apt install -y libzip-dev libpng-dev git npm \
    && docker-php-ext-install pdo_mysql gd zip \
    && curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer
RUN cd .. && git clone https://github.com/urlhum/UrlHum.git && a2enmod rewrite
WORKDIR /var/www/UrlHum
RUN cp .env-example .env && composer install \
    && php artisan key:generate && npm install && npm run dev \
    && sed -i 's|/var/www/html|/var/www/UrlHum/public|g' /etc/apache2/sites-available/000-default.conf \
    && chown -R www-data:www-data /var/www/UrlHum \
    && export OUT=$(php artisan migrate --force)
CMD [ "/bin/bash", "-c", "(sleep 5s; php artisan migrate --force; if [ '$OUT' != 'Nothing to migrate.' ]; then php artisan db:seed --force; else echo 'Nothing to seed.'; fi; php artisan settings:set) && apache2-foreground" ]
